package test.com.randomNumber.service;

import com.randomNumber.service.GenerateRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class GenerateRandomTest {

    GenerateRandom cut = new GenerateRandom();

    static Arguments[] getNextRandomArgs(){
        return new Arguments[]{
          Arguments.arguments(10,20),
          Arguments.arguments(-10,10),
          Arguments.arguments(25,55)
        };
    }

    @ParameterizedTest
    @MethodSource("getNextRandomArgs")
    void getNextRandom(int start,int end){
        int expected = cut.getNextRandom(start,end);
        System.out.print(expected);
        Assertions.assertTrue(start<expected&&expected<end);
    }
}