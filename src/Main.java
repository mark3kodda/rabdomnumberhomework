import com.randomNumber.service.GenerateRandom;

public class Main {
    public static void main(String[] args) {

        GenerateRandom generateRandom = new GenerateRandom();

        //1.Вывести на консоль случайное число.
        System.out.println("TASK 1");
        System.out.println(generateRandom.getNextRandom(0,10));

        //2.Вывести на консоль 10 случайных чисел.

        System.out.println("TASK 2");
        for (int i = 0; i <10 ; i++) {
            System.out.print((generateRandom.getNextRandom(20,40))+(" "));
        }

        System.out.println();

        //3.Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.

        System.out.println("TASK 3");
        for (int i = 0; i <10 ; i++) {
            System.out.print((generateRandom.getNextRandom(0,10))+(" "));
        }
        System.out.println();

        //4.Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.

        System.out.println("TASK 4");
        for (int i = 0; i <10 ; i++) {
            System.out.print((generateRandom.getNextRandom(20,50))+(" "));
        }
        System.out.println();

        //5.Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.

        System.out.println("TASK 5");
        for (int i = 0; i <10 ; i++) {
            System.out.print((generateRandom.getNextRandom(-10,10))+(" "));
        }
        System.out.println();

        //6.Вывести на консоль случайное количество (в диапазоне от 3 до 15)
        // случайных чисел, каждое в диапазоне от -10 до 35.

        System.out.println("TASK 6");
        int range = generateRandom.getNextRandom(3,15);
        System.out.println("Range is "+range);

        for (int i = 0; i < range; i++) {
            System.out.print((generateRandom.getNextRandom(-10,10))+(" "));
        }
        //System.out.println();

    }

}
